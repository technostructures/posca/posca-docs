---
sidebar_position: 1
---

# Deploy using Docker

We build [a Docker image](https://gitlab.com/technostructures/posca/posca/container_registry/4397972?orderBy=NAME&sort=desc&search[]=) that serves the application using Nginx.
The `latest` tag packages the tip of the `main` branch, semver tags match releases.

## Configuration

Environment variables are described [here](configuration).

## Deploy

This will run the unstable version. You should probably use [the last release](https://gitlab.com/technostructures/posca/posca/-/releases) as the image tag, and update when new versions are available.

```bash
docker run -d --restart=unless-stoped --name posca \
    -e POSCA_MATRIX_URL=https://matrix.example.com \
    -e POSCA_MATRIX_DOMAIN=example.com \
    registry.gitlab.com/technostructures/posca/posca:latest
```
