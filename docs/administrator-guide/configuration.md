---
sidebar_position: 2
---

# Configuration

Posca is configured using environment variables. It will refuse to start if the required variables are not provided.

## Default Matrix homeserver

### `POSCA_MATRIX_URL` **(required)**

The URL of the homeserver.

### `POSCA_MATRIX_DOMAIN` **(required)**

The domain (used in user and room IDs) of the homeserver.
