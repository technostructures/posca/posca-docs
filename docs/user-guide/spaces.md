---
sidebar_position: 2
---

# Spaces

A space is a collection of rooms and users. It can also contain other spaces.

## Space creation

To create a space, click on the dropdown menu in the sidebar, then on "Create a space".

On the next page, you can define the space name (mandatory) and its topic (a text describing the space – optional).
