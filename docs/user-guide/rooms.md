---
sidebar_position: 1
---

# Rooms

## Room creation

To create a room, click on the dropdown menu in the sidebar, then on "Create a room".

On the next page, you can define the room name (mandatory), its topic (a text introducing the topic of the room – optional), and the room type.

There are 4 different room types:

- Chat: a classic chat room;
- Forum: a forum, organized in threads, for long-term asynchronous discussion;
- Statuses: a microblogging experience;
- Media: a view made for sharing pictures and videos.
