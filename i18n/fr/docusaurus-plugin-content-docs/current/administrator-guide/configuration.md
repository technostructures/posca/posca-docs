---
sidebar_position: 2
---

# Configuration

Posca est configuré en utilisant des variables d'environment. L'application refusera de se lancer tant que les variables requises ne sont pas fournies.

## Homeserver Matrix par défaut

### `POSCA_MATRIX_URL` **(requis)**

L'URL du homeserver.

### `POSCA_MATRIX_DOMAIN` **(requis)**

Le domaine (utilisé dans les ID d'utilisateur·ices et de rooms) du homeserver.
