---
sidebar_position: 1
---

# Déploiement avec Docker

Nous fournissons [un image Docker](https://gitlab.com/technostructures/posca/posca/container_registry/4397972?orderBy=NAME&sort=desc&search[]=) qui sert l'application avec Nginx.
Le tag `latest` fournit le bout de la branche `main`, les tags semver correspondent aux releases.

## Configuration

Les variables d'environnement sont expliquées [ici](configuration).

## Déploiement

La commande suivante lancera la version instable. Vous devriez probablement utiliser [la dernière release](https://gitlab.com/technostructures/posca/posca/-/releases) comme tag d'image, et mettre à jour quand de nouvelles versions sont disponibles.

```bash
docker run -d --restart=unless-stoped --name posca \
    -e POSCA_MATRIX_URL=https://matrix.example.com \
    -e POSCA_MATRIX_DOMAIN=example.com \
    registry.gitlab.com/technostructures/posca/posca:latest
```
