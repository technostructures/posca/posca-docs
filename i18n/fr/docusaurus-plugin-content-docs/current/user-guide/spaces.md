---
sidebar_position: 2
---

# Spaces

Un space est une collection de rooms et d'utilisateur·ices. Il peut aussi contenir d'autres spaces.

## Création d'un space

Pour créer un space, cliquez sur le menu déroulant dans la barre latérale, puis sur « Créer un space ».

Sur la page suivante, vous pouvez définir le nom du space (obligatoire) et son thème (un texte décrivant l'espace - facultatif).
