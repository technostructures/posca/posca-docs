---
sidebar_position: 1
---

# Rooms

## Création de room

Pour créer une room, cliquez sur le menu déroulant dans la barre latérale, puis sur « Créer une room ».

Sur la page suivante, vous pouvez définir le nom de la room (obligatoire), son sujet (un texte présentant le sujet de la room - facultatif), et le type de room.

Il existe 4 types de room différents :

- Chat : un salon de discussion classique ;
- Forum : un forum, organisé en fils de discussion, pour des discussions asynchrones à long terme ;
- Statuts : une expérience de microblogging ;
- Media : une vue conçue pour le partage d'images et de vidéos.
